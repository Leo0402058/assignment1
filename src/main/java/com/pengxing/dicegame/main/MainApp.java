package com.pengxing.dicegame.main;


import com.pengxing.dicegame.controller.DiceGameFXMLController;
import com.pengxing.dicegame.controller.GamePanelFXMLController;
import com.pengxing.dicegame.data.UserBean;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

/**
 * Example of a form created in FXML and with a controller - In this approach
 * there is no need for a Presentation class as the FXML/Controller take over
 * this role.
 *
 * @author Ken
 */
public class MainApp extends Application {

    /**
     * Start the JavaFX application
     *
     * @param primaryStage
     */
    
    DiceGameFXMLController welcomegui;
    @Override
    public void start(Stage primaryStage) throws Exception {
        
   
        welcomegui = new DiceGameFXMLController();
        UserBean bean = new UserBean();
        Parent root;
       //GamePanelFXMLController gamegui;
        
        UserBean userBean = new UserBean();
        Scene scene;
        
        welcomegui.start(primaryStage);
//        try {
//            
//            // Create a loader object for the FXML file
//            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/DiceGameFXML.fxml"));
//
//           
//
//            // If objects that can only be craeted in the FXML controller must be
//            // retrieved or if messages must sent to the controller we need the
//            // following code
//            // Retrieve a reference to the FXML controller so that we may
//            // communicate with it. In this case we will use it to get the UserBean
//            controller1 = loader.getController();
//             // Use the loader to retrieve the root container. Using the container
//            // superclass Parent means any container is compatible
//            root = loader.load();
//
//            // Retrieve the UserBean so we can use it in other objects
//            //UserBean userBean = controller.getUserBean();
//
//             scene = new Scene(root);
//
//            primaryStage.setTitle("FXML Form 03");
//            primaryStage.setScene(scene);
//            primaryStage.show();
           
            
            
            
           
//        } catch (IOException | IllegalStateException ex) {
//            Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
//            
//            //errorAlert(ex.getMessage());
//        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle("Program Start Error");
        dialog.setHeaderText("ERROR");
        dialog.setContentText(msg);
        dialog.show();

    }

    /**
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
