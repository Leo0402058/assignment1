/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pengxing.dicegame.controller;

import com.pengxing.dicegame.data.DiceBean;

/**
 *
 * @author User
 */
public class PassLine{
    DiceBean dicebean;
    int total = 0;
   static int last_point =0;
   static int count =0;
    
    public int PassLine( DiceBean db, double betAmount, int firstDice, int secondDice){
       dicebean = db;
       total = firstDice + secondDice;
       dicebean.setBetAmount(betAmount);
      // dicebean.setResultmsg(gameStatus(total));
       last_point = total;
       int status = 0;
       this.total = total;
       count ++;
           if(count==1){
             if(total == 7 || total == 11){
                isWin();
                status= 1;
                
             }else if(total==2||total==3||total==12){
                isLoose();
                status= 0;
                
             }else{
                     
                 isContinue();
                  status= 2;
            }
          }else if(count>1){
             if(total == 7){
                
                 isLoose();
                 status= 0;
                
             }else if(total == last_point){
               isWin();
               status= 1;
             }else{
                     
               status= 2;
            }
          
          }
      
        return status;
    }
    
    
     
     
     public void isWin(){

        dicebean.setBalance(dicebean.getBetAmount()+dicebean.getBalance());
        String msg = "You have win " + dicebean.getBetAmount()+"dollars";
        String title = "Congratulations!!";
        MessageBox.show(msg,title);
        
       
     }
     
     public void isLoose(){

        dicebean.setBalance(dicebean.getBalance()-dicebean.getBetAmount()); 
        String msg = "You have lose your bet: " + dicebean.getBetAmount()+" dollars"; 
        String title = "Sorry!!";
        MessageBox.show(msg,title);
        
       
     }
     
     public void isContinue(){
        String msg = "Continue to Roll"; 
        String title = "Continue!!";
        MessageBox.show(msg,title);
     }

}       
    

