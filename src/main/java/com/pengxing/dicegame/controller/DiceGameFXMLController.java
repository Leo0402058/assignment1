/**
 * Sample Skeleton for 'DiceGameFXML.fxml' Controller Class
 */

package com.pengxing.dicegame.controller;

import com.pengxing.dicegame.data.DiceBean;
import com.pengxing.dicegame.data.UserBean;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class DiceGameFXMLController {
      private UserBean userBean;
      Stage prevStage;
      double betAmount;
      FormatChecker formatchecker = new FormatChecker();


    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="usernametxt"
    private TextField usernametxt; // Value injected by FXMLLoader

    @FXML // fx:id="emailtext"
    private TextField emailtext; // Value injected by FXMLLoader

    @FXML // fx:id="regbtn"
    private Button regbtn; // Value injected by FXMLLoader

    @FXML // fx:id="startbtn"
    private Button startbtn; // Value injected by FXMLLoader

    @FXML // fx:id="exitbtn"
    private Button exitbtn; // Value injected by FXMLLoader

    @FXML // fx:id="amounttxt1"
    private TextField amounttxt1; // Value injected by FXMLLoader

    @FXML // fx:id="passwordField"
    private PasswordField passwordField; // Value injected by FXMLLoader

    @FXML // fx:id="prompttext"
    private Label prompttext; // Value injected by FXMLLoader

    @FXML
    void exitclick(ActionEvent event) {
                       Platform.exit();
    }
     DiceBean dbean = new DiceBean();

    @FXML  /* the action of register button,including examine the rules of 
             textfield which user input their information*/
    boolean registerclick(ActionEvent event) {
              
             boolean reg = false;
             if(formatchecker.usernameChecker(usernametxt.getText())==true&&
             formatchecker.passwordChecker(passwordField.getText())==true&&
             formatchecker.emailChecker(emailtext.getText())==true&&
             formatchecker.betAmountChecker(Double.parseDouble(amounttxt1.getText()))==true)
             {
                 reg = true;
                 String msg = "Congratulations!! Game Is Sucessfully Registered.\n"
                    + "Start Game Now!!";
                 String title = "Registration Sucess!";
                 MessageBox.show(msg, title);
                 reg = true;
             }else{
                    reg = false;
             }           
            
             
 
           return reg;
    }
     
    public void setPrevStage(Stage stage){
         this.prevStage = stage;
    }
    
   
   
    public void promptInfo(){
        String usernameFormat=  "Please make sure your username meet the following requirements: \n"
                +"1. Username cannot be empty.\n"
                + "2. Please DO NOT include the special letter into your username.\n"
                + "3. Please make sure your username consist of numbers and letters \n"
                + "4. Please make sure your username length is less than 10 characters\n"
                + "5. Please make sure your username consist at least 1 uppercase and lowercase letter";
        String passwordFormat = 
                "Please make sure your passwords meet the following requirements: \n"
                +" 1. password cannot be empty.\n"
                + "2. Please including the special letter into your password.\n"
                + "3. Please make sure your password consist of numbers and letters \n"
                + "4. Please make sure your password length is less than 10 characters \n"
                + "5. Please make sure your password consist at least 1 uppercase and lowercase letter";
               ;
        String emailFormat =
                "Please make sure your email address meet the following requirements: \n"
                 +"1. Email address cannot be empty.\n"
                + "2. Please DO NOT include the special letter into your email address.\n"
                + "3. Make sure you enter a valid email";

        
       if(usernametxt.isFocused()){usernametxt.setPromptText(usernameFormat);
       
       }else if(passwordField.isFocused()){passwordField.setPromptText(passwordFormat);
       
       }else if(emailtext.isFocused()){emailtext.setPromptText(emailFormat);}
    
    }
    
    public double passBetAmount(){
        
        return dbean.getBetAmount();
    }
    
    @FXML
      void startclick(ActionEvent event)  throws IOException{
      
       if(registerclick(event)==true){
       
               Stage stage = new Stage();
               stage.setTitle("Dice Game Verson 1.0 Game Panel");
               Pane myPane = null;
               myPane = FXMLLoader.load(getClass().getResource("/fxml/GamePanelFXML.fxml"));
               Scene scene = new Scene(myPane);
               stage.setScene(scene);
               prevStage.close();
               stage.show();
      }else{
               prompttext.setText("Please register first");
       
      }
      }
   
      public void start(Stage primaryStage) throws Exception {
  
          primaryStage.setTitle("Dice Game Verson 1.0");

          FXMLLoader myLoader = new FXMLLoader(getClass().getResource("/fxml/DiceGameFXML.fxml"));

          Pane myPane = (Pane)myLoader.load();

          DiceGameFXMLController controller = (DiceGameFXMLController) myLoader.getController();

          controller.setPrevStage(primaryStage);

          Scene myScene = new Scene(myPane);        
          primaryStage.setScene(myScene);
          primaryStage.show();
   
   }
   
    
    

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert usernametxt != null : "fx:id=\"usernametxt\" was not injected: check your FXML file 'DiceGameFXML.fxml'.";
        assert emailtext != null : "fx:id=\"emailtext\" was not injected: check your FXML file 'DiceGameFXML.fxml'.";
        assert regbtn != null : "fx:id=\"regbtn\" was not injected: check your FXML file 'DiceGameFXML.fxml'.";
        assert startbtn != null : "fx:id=\"startbtn\" was not injected: check your FXML file 'DiceGameFXML.fxml'.";
        assert exitbtn != null : "fx:id=\"exitbtn\" was not injected: check your FXML file 'DiceGameFXML.fxml'.";
        assert amounttxt1 != null : "fx:id=\"amounttxt1\" was not injected: check your FXML file 'DiceGameFXML.fxml'.";
        assert passwordField != null : "fx:id=\"passwordField\" was not injected: check your FXML file 'DiceGameFXML.fxml'.";
        assert prompttext != null : "fx:id=\"prompttext\" was not injected: check your FXML file 'DiceGameFXML.fxml'.";

    }
}
