/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pengxing.dicegame.controller;

import com.pengxing.dicegame.data.DiceBean;

/**
 *
 * @author User
 */
public class FieldBet{
    DiceBean dicebean = new DiceBean();
    int total = 0;
    public boolean FieldBet(DiceBean db,double betAmount, int firstDice, int secondDice){
       total = firstDice + secondDice;
       dicebean = db;
       dicebean.setBetAmount(betAmount);
       boolean status = false;
       boolean win = false;
           if(total == 3||total == 4||total == 9||total == 10||total ==11){  
              isWin(1);
              status =  true;
           }else if(total == 2){
              
               isWin(2);
               status =  true;
          }else if(total == 12){
              
               isWin(3);
               status =  true;
          }else{
               
               isLoose();
               status = false;
          }
      
        return status; 
    }
    
    
    
     public void isWin(int n){

        dicebean.setBalance(n*dicebean.getBetAmount()+dicebean.getBalance());
        String msg = "You have win "+ n + "times of Your Bet:" + n*dicebean.getBetAmount()+"dollars";
        String title = "Congratulations!!";
        MessageBox.show(msg,title);
        
       
     }
     
     public void isLoose(){

        dicebean.setBalance(dicebean.getBalance()-dicebean.getBetAmount()); 
        String msg = "You have lose your bet: " + dicebean.getBetAmount()+" dollars"; 
        String title = "Sorry!!";
        MessageBox.show(msg,title);
        
       
     }
     
  

}
