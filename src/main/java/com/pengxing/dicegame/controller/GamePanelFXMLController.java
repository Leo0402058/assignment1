/**
 * Sample Skeleton for 'GamePanelFXML.fxml' Controller Class
 */

package com.pengxing.dicegame.controller;


import com.pengxing.dicegame.data.DiceBean;
import com.pengxing.dicegame.data.UserBean;
import javafx.scene.image.Image;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class GamePanelFXMLController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="flineradion"
    private RadioButton flineradion; // Value injected by FXMLLoader

    @FXML // fx:id="gamemode"
    private ToggleGroup gamemode; // Value injected by FXMLLoader

    @FXML // fx:id="betradion"
    private RadioButton betradion; // Value injected by FXMLLoader

    @FXML // fx:id="any7radio"
    private RadioButton any7radio; // Value injected by FXMLLoader

    @FXML // fx:id="amountbettxt"
    private TextField amountbettxt; // Value injected by FXMLLoader

    @FXML // fx:id="confirmbtn"
    private Button confirmbtn; // Value injected by FXMLLoader

    @FXML // fx:id="cancelbtn"
    private Button cancelbtn; // Value injected by FXMLLoader

    @FXML // fx:id="rollGame"
    private AnchorPane rollGame; // Value injected by FXMLLoader

    @FXML // fx:id="rollhandle"
    private Button rollhandle; // Value injected by FXMLLoader

    @FXML // fx:id="resultshowtxt"
    private Label resultshowtxt; // Value injected by FXMLLoader

    @FXML // fx:id="playertxt"
    private Label playertxt; // Value injected by FXMLLoader

    @FXML // fx:id="fundtxt"
    private Label fundtxt; // Value injected by FXMLLoader

    @FXML // fx:id="betshowtxt"
    private Label betshowtxt; // Value injected by FXMLLoader

    @FXML // fx:id="imageOne"
    private ImageView imageOne; // Value injected by FXMLLoader

    @FXML // fx:id="imageTwo"
    private ImageView imageTwo; // Value injected by FXMLLoader
    
    
   DiceBean dicebean = new DiceBean();
   DiceGameFXMLController dicecontrol = new DiceGameFXMLController();
   static int count = 0;
   boolean checker = false;
   double betAmount = 0.0;
   
   
   Image imgOne = new Image("/images/1.png");
   Image imgTwo = new Image("/images/2.png");
   Image imgThree = new Image("/images/3.png");
   Image imgFour = new Image("/images/4.png");
   Image imgFive = new Image("/images/5.png");
   Image imgSix = new Image("/images/6.png");
    
    private double balance;
    
   public void GamePanelFXMLController(double balance,double betAmount){
      this.balance = balance;
      balance = 500;
      
      
      
   }
   

    @FXML
    void cancelBet(ActionEvent event) {
       dicebean.setBalance(dicebean.getBalance()-dicebean.getBetAmount());
       dicebean.setBetAmount(0.0);
       amountbettxt.clear();
       betshowtxt.setText("0.00");
       controlDisabled();
    }

    @FXML
    void confirmBet(ActionEvent event) {
          //disable roll button before confirm bet amount
          if(dicebean.getBetAmount()>0){
                rollhandle.setDisable(false);
                betshowtxt.setText(String.valueOf(dicebean.getBetAmount())); 
          }else {
              if(Double.parseDouble(amountbettxt.getText())>=10.0){
                dicebean.setBetAmount(Double.parseDouble(amountbettxt.getText())+dicebean.getBetAmount());
                betshowtxt.setText(String.valueOf(dicebean.getBetAmount()));
                amountbettxt.clear();
                rollhandle.setDisable(false);
                confirmbtn.setDisable(true);
                amountbettxt.setDisable(true);
                
              }else{
                String msg = "You Need Set At Least 10$ To Start!!";
                String title = "Bet Too Low!";
                MessageBox.show(msg, title);
                rollhandle.setDisable(true);
              }
          }
    }
          
         
          
          
        
  

    @FXML
    void rollStart(ActionEvent event) {
       
       if(flineradion.isSelected()){

           Random rn = new Random();
           dicebean.setDieOne(rn.nextInt(6)+1);
           dicebean.setDieTwo(rn.nextInt(6)+1);
           imageChoose();
           startPassLine(dicebean.getBetAmount(),dicebean.getDieOne(),dicebean.getDieTwo());
           
       }else if(betradion.isSelected()){
           
           Random rn = new Random();
           dicebean.setDieOne(rn.nextInt(6)+1);
           dicebean.setDieTwo(rn.nextInt(6)+1);
           imageChoose();
           startFieldBet(dicebean.getBetAmount(),dicebean.getDieOne(),dicebean.getDieTwo());
           
       }else if(any7radio.isSelected()){
           
           Random rn = new Random();
           dicebean.setDieOne(rn.nextInt(6)+1);
           dicebean.setDieTwo(rn.nextInt(6)+1);
           imageChoose();
           startAny7(dicebean.getBetAmount(),dicebean.getDieOne(),dicebean.getDieTwo());
           

      }
    }
    public boolean rollChecker(double betAmount, double balance){
        
        
        if((balance-betAmount)>0){
            checker= true;
        }else if(betAmount>10){
            String msg = "You don't have enough fund to continue!\n"
                    + "Please deposit minimum 10$ to play!!";
            String title = "Low Fund Waring!";
            MessageBox.show(msg, title);
            rollhandle.setDisable(true);
            checker =false;    
        }
       
        return checker;
    }
    
   
    
    
    public void imageChoose(){


          //Set image sequence for imageView Two
          
          if(dicebean.getDieOne()==1){
              imageOne.setImage(imgOne);
          }else if(dicebean.getDieOne()==2){
              imageOne.setImage(imgTwo);
          }else if(dicebean.getDieOne()==3){
              imageOne.setImage(imgThree);
          }else if(dicebean.getDieOne()==4){
              imageOne.setImage(imgFour);
           }else if(dicebean.getDieOne()==5){
              imageOne.setImage(imgFive);
           }else if(dicebean.getDieOne()==6){
              imageOne.setImage(imgSix);
           }
          
           //Set image sequence for imageView one
            if(dicebean.getDieTwo()==1){
              imageTwo.setImage(imgOne);
          }else if(dicebean.getDieTwo()==2){
              imageTwo.setImage(imgTwo);
          }else if(dicebean.getDieTwo()==3){
              imageTwo.setImage(imgThree);
          }else if(dicebean.getDieTwo()==4){
              imageTwo.setImage(imgFour);
           }else if(dicebean.getDieTwo()==5){
              imageTwo.setImage(imgFive);
           }else if(dicebean.getDieTwo()==6){
              imageTwo.setImage(imgSix);
           }
            
            

          
   }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert flineradion != null : "fx:id=\"flineradion\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert gamemode != null : "fx:id=\"gamemode\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert betradion != null : "fx:id=\"betradion\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert any7radio != null : "fx:id=\"any7radio\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert amountbettxt != null : "fx:id=\"amountbettxt\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert confirmbtn != null : "fx:id=\"confirmbtn\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert cancelbtn != null : "fx:id=\"cancelbtn\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert rollGame != null : "fx:id=\"rollGame\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert rollhandle != null : "fx:id=\"rollhandle\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert resultshowtxt != null : "fx:id=\"resultshowtxt\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert playertxt != null : "fx:id=\"playertxt\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert fundtxt != null : "fx:id=\"fundtxt\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert betshowtxt != null : "fx:id=\"betshowtxt\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert imageOne != null : "fx:id=\"imageOne\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        assert imageTwo != null : "fx:id=\"imageTwo\" was not injected: check your FXML file 'GamePanelFXML.fxml'.";
        
        
       fundtxt.setText(String.valueOf(500));
       betshowtxt.setText("50.0");
       dicebean.setBalance(500);
       dicebean.setBetAmount(50);
       controlDisabled();
      
       
    }
    
    public void controlDisabled(){
        if(dicebean.getBetAmount()==0){
           rollhandle.setDisable(true);
           betshowtxt.setText("0.0");
           confirmbtn.setDisable(false);
           amountbettxt.setDisable(false);
           cancelbtn.setDisable(false);
       }else{
           confirmbtn.setDisable(true);
           amountbettxt.setDisable(true);
           cancelbtn.setDisable(false);
        }
    
    }

    private void startPassLine(double betAmount, int firstDice, int scendDice) {
        PassLine passline = new PassLine();
        
        switch(passline.PassLine(dicebean, betAmount, firstDice, scendDice)){
            case 0: amountbettxt.setText("0.0");
                    dicebean.setBetAmount(0.0);
                    fundtxt.setText(String.valueOf(dicebean.getBalance()));
                    controlDisabled();
                    break;
            case 1: amountbettxt.setText("0.0");
                    dicebean.setBetAmount(0.0);
                    fundtxt.setText(String.valueOf(dicebean.getBalance()));
                    controlDisabled();
                    break;
            case 2: fundtxt.setText(String.valueOf(dicebean.getBalance()));
                    break;
        };
       
        
    }

    // set method for game FleldBet to start
    private void startFieldBet(double betAmount, int firstDice, int scendDice) {
        FieldBet fieldbet = new FieldBet();
       
        if(fieldbet.FieldBet(dicebean,betAmount, firstDice, scendDice)==true){
                    amountbettxt.setText("0.0");
                    dicebean.setBetAmount(0.0);
                    fundtxt.setText(String.valueOf(dicebean.getBalance()));
                    controlDisabled();
        
        }else{
                    amountbettxt.setText("0.0");
                    dicebean.setBetAmount(0.0);
                    fundtxt.setText(String.valueOf(dicebean.getBalance()));
                    controlDisabled();
                    
        
        };  
    }
    // set method for game any7 to start
    private void startAny7(double betAmount,int firstDice,int scendDice) {
        Any7 any7 = new Any7();
        
             any7.Any7(dicebean, betAmount, firstDice, scendDice, count);
             amountbettxt.setText("0.0");
             dicebean.setBetAmount(0.0);
             fundtxt.setText(String.valueOf(dicebean.getBalance()));
             controlDisabled();
        
        }
}
