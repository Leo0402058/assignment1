/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pengxing.dicegame.data;

import java.util.Random;
import javafx.beans.property.IntegerProperty;

/**
 *
 * @author User
 */
public class DiceBean {
    private int dieOne;
    private int dieTwo;
    private double betAmount;
    private double balance;
    private String resultmsg;

   
    
     public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
    
    public double getBetAmount() {
        return betAmount;
    }

    public void setBetAmount(double betAmount) {
        this.betAmount = betAmount;
    }
    
    
      public int getDieTwo() {
        return dieTwo;
    }

    public void setDieTwo(int die) {
        this.dieTwo = die;
        //System.out.print(die);
    }
    
    

    public int getDieOne() {
        return dieOne;
    }

    public void setDieOne(int die) {
        this.dieOne = die;
        //System.out.print(die);
    }
    
    public void setRandomValue(){
          Random rn = new Random();
            
        this.setDieOne(rn.nextInt(6)+1);
        this.setDieTwo(rn.nextInt(6)+1);
    }

    public String getResultmsg() {
        return resultmsg;
    }

    public void setResultmsg(String resultmsg) {
        this.resultmsg = resultmsg;
    }
  

   
    
}
